package com.portal.base.authenticate.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.portal.base.authenticate.entity.SysPosition;
import com.portal.base.authenticate.mapper.SysPositionMapper;
import com.portal.base.authenticate.service.ISysPositionService;
import org.springframework.stereotype.Service;

/**
 * @Description: 职务表
 * @Author: jeecg-boot
 * @Date: 2019-09-19
 * @Version: V1.0
 */
@Service
public class SysPositionServiceImpl extends ServiceImpl<SysPositionMapper, SysPosition> implements ISysPositionService {

}
