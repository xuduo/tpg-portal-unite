package com.portal.base.authenticate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.portal.base.authenticate.entity.SysRolePermission;

/**
 * <p>
 * 角色权限表 Mapper 接口
 * </p>
 *
 * @Author scott
 * @since 2018-12-21
 */
public interface SysRolePermissionMapper extends BaseMapper<SysRolePermission> {

}
