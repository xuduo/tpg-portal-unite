package com.portal.base.authenticate.service;

import com.alibaba.fastjson.JSONObject;
import com.portal.base.core.vo.Result;
import com.portal.base.user.entity.SysUser;

import javax.servlet.http.HttpServletRequest;
import java.util.Set;

public interface AbstractUserPermission {
    /**
     * 通过用户名获取用户权限集合
     *
     * @param username 用户名
     * @return 权限集合
     */
    Set<String> getUserPermissionsSet(String username);
    /**
     * 用户登陆系统并办法token
     * @param username 用户名
     * @param password 密码
     * @return
     */
     Result<JSONObject> userLogin(String username, String password);
    /**
     * 用户退出
     * @param token
     * @return
     */
    public Result<Object> logout( String token);
    /**
     *  从请求中获取token,前端请求需要添加X-Access-Token头信息
     * @param request
     * @return token
     */
    String getToken(HttpServletRequest request);

}
