package com.portal.base.authenticate.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.portal.base.authenticate.entity.SysPosition;

/**
 * @Description: 职务表
 * @Author: jeecg-boot
 * @Date: 2019-09-19
 * @Version: V1.0
 */
public interface ISysPositionService extends IService<SysPosition> {

}
