package com.portal.base.security.sm2;

import com.portal.base.security.util.Util;
import javafx.scene.paint.Stop;
import org.bouncycastle.util.encoders.Base64;

import java.io.IOException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Scanner;


public class SM2Test {

    public static void main(String[] args) throws IOException
    {
       String[] arr_keys = SM2Utils.generateKeyPair();
        System.out.println(  arr_keys[0]);
        System.out.println(  arr_keys[1]);
//        System.out.print(  UUID.randomUUID().toString().replaceAll("-", ""));

        System.out.print("输入明文: ");
        Scanner input=new Scanner(System.in);
        String plainText=input.nextLine();
        byte[] sourceData = plainText.getBytes();

        // 国密规范测试私钥
        String prik = "128B2FA8BD433C6C068C8D803DFF79792A519A55171B1B650C23661D15897263";
        prik=arr_keys[1];
        String prikS = new String(Base64.encode(Util.hexToByte(prik)));
        System.out.println("prikS: " + prikS);
        System.out.println("");

        // 国密规范测试用户ID
        String userId = "ALICE123@YAHOO.COM";

        System.out.println("ID: " + Util.getHexString(userId.getBytes()));
        System.out.println("");

        System.out.println("签名: ");
        byte[] c = SM2Utils.sign(userId.getBytes(), Base64.decode(prikS.getBytes()), sourceData);
        System.out.println("sign: " + Util.getHexString(c));
        System.out.println("");

        // 国密规范测试公钥
        String pubk = "040AE4C7798AA0F119471BEE11825BE46202BB79E2A5844495E97C04FF4DF2548A7C0240F88F1CD4E16352A73C17B7F16F07353E53A176D684A9FE0C6BB798E857";
        pubk = arr_keys[0];
        String pubkS = new String(Base64.encode(Util.hexToByte(pubk)));
        System.out.println("pubkS: " + pubkS);
        System.out.println("");


        System.out.println("验签: ");
       boolean vs = SM2Utils.verifySign(userId.getBytes(), Base64.decode(pubkS.getBytes()), sourceData, c);
        System.out.println("验签结果: " + vs);
        System.out.println("");

        System.out.println("加密: ");
        byte[] cipherText = SM2Utils.encrypt(Base64.decode(pubkS.getBytes()), sourceData);
        System.out.println(new String(Base64.encode(cipherText)));
        System.out.println("");

        System.out.println("解密: ");
        plainText = new String(SM2Utils.decrypt(Base64.decode(prikS.getBytes()), cipherText));
        System.out.println(plainText);
    }




}
