package com.portal.base.security.sm3;

import org.bouncycastle.util.encoders.Hex;

import java.io.IOException;
import java.util.Scanner;

public class SM3Test {

    public static void main(String[] args) throws IOException
    {
        System.out.print("输入明文: ");
        Scanner input=new Scanner(System.in);
        String plainText=input.nextLine();
        byte[] md = new byte[32];
        byte[] msg1 = plainText.getBytes();
        SM3Digest sm3 = new SM3Digest();
        sm3.update(msg1, 0, msg1.length);
        sm3.doFinal(md, 0);
        String s = new String(Hex.encode(md));
        System.out.println(s);
    }
}
