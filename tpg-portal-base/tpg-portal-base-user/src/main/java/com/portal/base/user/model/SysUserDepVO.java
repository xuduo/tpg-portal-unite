package com.portal.base.user.model;

import lombok.Data;

/**
 * @Author qinfeng
 * @Date 2020/1/2 21:58
 * @Description:
 * @Version 1.0
 */
@Data
public class SysUserDepVO {
    private String userId;
    private String departName;
}
