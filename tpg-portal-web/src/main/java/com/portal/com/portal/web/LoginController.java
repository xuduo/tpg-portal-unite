package com.portal.com.portal.web;

import com.portal.base.authenticate.config.shiro.JwtToken;
import com.portal.base.authenticate.service.AbstractUserPermission;
import com.portal.base.authenticate.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @program: tpg-portal-parent
 * @description: 登陆测试
 * @author: xuduo
 * @create: 2020-05-11 22:33
 * @Version: 1.0
 **/
@RestController
public class LoginController {
    @Autowired
    AbstractUserPermission abstractUserPermission;

    @GetMapping("/sys/login")
    public  Object login(String name,String pwd){
      return   abstractUserPermission.userLogin(name,pwd);
    }
    @GetMapping("/sys/loginOut")
    public Object loginOut(HttpServletRequest request){
        String token = abstractUserPermission.getToken(request);
       return abstractUserPermission.logout(token);
    }
    @GetMapping("/sys/test")
    public String test(){
        return "test";
    }
}
