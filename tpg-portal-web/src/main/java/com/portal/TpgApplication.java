package com.portal;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(value={"com.portal.**.mapper*"})
public class TpgApplication {
    public static void main(String[] args) {
        SpringApplication.run(TpgApplication.class,args);
    }
}
