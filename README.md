# tpg-portal

#### 介绍
综合开发平台

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
5.  test

#### 目录介绍
![目录介绍](./readmeImages/mulujieshao.png)

* **tpg-portal-base**:基础公共调用。
1. **tpg-portal-base-user**:用户相关操作
2. **tpg-portal-base-org**:组织相关操作
3. **tpg-portal-base-authenticate**:用户登陆登出、权限验证配置等。
* **tpg-portal-sso** ：集成登陆，集成cas客户端登陆。与cas服务器单点支持restful方式和调用casClent客户端api两种方式。
* **tpg-portal-log**：使用log4j规范，可以记录开发日志、审计日志、系统间调试日志或叫日志跟踪。